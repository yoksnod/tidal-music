package com.example.tidal

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Rule

@ExperimentalCoroutinesApi
open class BaseCoroutineTest {
    @Rule
    @JvmField
    val instantTaskExecutor = InstantTaskExecutorRule()

    @get:Rule
    val coroutineRule = MainCoroutineRule()

    val dispatcher = coroutineRule.testDispatcher
}