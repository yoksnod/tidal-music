package com.example.tidal

import com.example.app.view.ThrottledTextWatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test

private const val TEST_DELAY = 1000L

@ExperimentalCoroutinesApi
class ThrottledTextWatcherTest : BaseCoroutineTest() {

    private var query: String? = null
    private val textWatcher = ThrottledTextWatcher(TestCoroutineScope(dispatcher), ioDispatcher = dispatcher) {
        query = it
    }

    @Before
    fun setup() {
        query = null
    }

    @Test
    fun `onTextChanged repeatedly - only the last query will be called`() {
        dispatcher.runBlockingTest {
            textWatcher.onTextChanged("x", 0, 0, 0)
            textWatcher.onTextChanged("y", 0, 0, 0)
            textWatcher.onTextChanged("z", 0, 0, 0)
            delay(TEST_DELAY)
            Assert.assertEquals("z", query)
        }
    }

    @Test
    fun `onTextChanged repeatedly after a delay - only query before the delay will matter`() {
        dispatcher.runBlockingTest {
            textWatcher.onTextChanged("y", 0, 0, 0)
            textWatcher.onTextChanged("z", 0, 0, 0)
            delay(TEST_DELAY)
            textWatcher.onTextChanged("x", 0, 0, 0)
            Assert.assertEquals("z", query)
        }
    }

    @Test
    fun `onTextChanged whitespaces only - should not be called at all`() {
        dispatcher.runBlockingTest {
            textWatcher.onTextChanged("   ", 0, 0, 0)
            delay(TEST_DELAY)
            Assert.assertEquals(null, query)
        }
    }

    @Test
    fun `onTextChanged text and whitespaces - delete whitespaces`() {
        dispatcher.runBlockingTest {
            textWatcher.onTextChanged(" aaaa ", 0, 0, 0)
            delay(TEST_DELAY)
            Assert.assertEquals("aaaa", query)
        }
    }

    @Test
    fun `onTextChanged null query - should not be called at all`() {
        query = "z"
        dispatcher.runBlockingTest {
            textWatcher.onTextChanged(null, 0, 0, 0)
            Assert.assertEquals("z", query)
        }
    }
}