package com.example.app.viewmodel

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.app.data.AlbumsRepo
import com.example.app.entity.Album
import com.example.app.entity.AlbumDetailsResult
import com.example.app.utils.BUNDLE_ALBUM
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class AlbumDetailsViewModel(
    private val albumsRepo: AlbumsRepo,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    defaultArgs: Bundle?
) : ViewModel() {
    private val album: Album = defaultArgs?.getParcelable(BUNDLE_ALBUM) ?: throw RuntimeException("must be not null")
    internal var detailsLiveData: LiveData<AlbumDetailsResult>? = null

    fun requestAlbumDetails(): LiveData<AlbumDetailsResult> {
        val liveData = liveData(ioDispatcher) {
            emit(albumsRepo.getAlbumDetails(albumId = album.id))
        }
        detailsLiveData = liveData
        return liveData
    }
}