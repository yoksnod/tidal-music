package com.example.app.viewmodel

import android.os.Bundle
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.savedstate.SavedStateRegistryOwner
import com.example.app.data.AlbumsRepo

class TidalViewModelFactory(
    private val albumsRepo: AlbumsRepo,
    owner: SavedStateRegistryOwner,
    private val defaultArgs: Bundle?
) : AbstractSavedStateViewModelFactory(
    owner,
    defaultArgs
) {
    override fun <T : ViewModel> create(key: String, modelClass: Class<T>, handle: SavedStateHandle): T {
        if (modelClass.isAssignableFrom(SearchSuggestionsViewModel::class.java)) {
            return SearchSuggestionsViewModel(albumsRepo) as T
        }

        if (modelClass.isAssignableFrom(AlbumsViewModel::class.java)) {
            return AlbumsViewModel(albumsRepo) as T
        }
        if (modelClass.isAssignableFrom(AlbumDetailsViewModel::class.java)) {
            return AlbumDetailsViewModel(albumsRepo, defaultArgs = defaultArgs) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class")
    }
}