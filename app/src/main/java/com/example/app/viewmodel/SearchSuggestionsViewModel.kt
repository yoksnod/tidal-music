package com.example.app.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.app.data.AlbumsRepo
import com.example.app.entity.SuggestionsResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

class SearchSuggestionsViewModel(
    private val albumsRepo: AlbumsRepo,
    ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {
    private val coroutineScope: CoroutineScope = CoroutineScope(ioDispatcher)
    private lateinit var suggestionsLiveData: LiveData<SuggestionsResult>
    private lateinit var currentJob: Job
    private var lastQuery: String? = null

    fun requestSuggestions(params: String): LiveData<SuggestionsResult> {
        if (lastQuery == params) return suggestionsLiveData
        lastQuery = params
        if (this::currentJob.isInitialized) currentJob.cancel()
        currentJob = Job()
        suggestionsLiveData = liveData(coroutineScope.coroutineContext + currentJob) {
            emit(albumsRepo.getSuggestions(params))
        }
        return suggestionsLiveData
    }
}