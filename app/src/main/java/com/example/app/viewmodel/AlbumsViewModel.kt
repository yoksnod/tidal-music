package com.example.app.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.example.app.data.AlbumsRepo
import com.example.app.entity.AlbumsResult
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

class AlbumsViewModel(
    private val albumsRepo: AlbumsRepo,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : ViewModel() {
    internal var albumsLiveData: LiveData<AlbumsResult>? = null

    fun requestAlbums(artistId: Long): LiveData<AlbumsResult> {
        val liveData = liveData(ioDispatcher) {
            emit(albumsRepo.getAlbums(artistId))
        }
        albumsLiveData = liveData
        return liveData
    }
}