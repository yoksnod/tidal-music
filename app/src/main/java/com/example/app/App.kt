package com.example.app

import android.app.Application
import android.content.Context
import com.example.app.data.AlbumDetailsDataSourceImpl
import com.example.app.data.AlbumsDataSourceImpl
import com.example.app.data.AlbumsRepo
import com.example.app.data.AlbumsRepoImpl
import com.example.app.data.SearchSuggestionsDataSourceImpl
import com.example.app.net.RestProvider

class App : Application() {

    lateinit var albumsRepo: AlbumsRepo
        private set

    private lateinit var restProvider: RestProvider

    override fun onCreate() {
        super.onCreate()
        restProvider = RestProvider(this)
        albumsRepo = AlbumsRepoImpl(
            SearchSuggestionsDataSourceImpl(restProvider.restService),
            AlbumsDataSourceImpl(restProvider.restService),
            AlbumDetailsDataSourceImpl(restProvider.restService)
        )
    }
}

fun Context.app(): App = applicationContext as App