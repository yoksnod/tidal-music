package com.example.app.entity

sealed class AlbumDetailsResult {
    data class Success(val details: AlbumDetails) : AlbumDetailsResult()
    data class Fail(val exception: Exception) : AlbumDetailsResult()
}

