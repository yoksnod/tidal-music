package com.example.app.entity

import com.google.gson.annotations.SerializedName

data class Track(
    val id: Long,
    val rank: Long,
    val duration: Long,
    val preview: String,
    val title: String,
    @SerializedName("title_short")
    val titleShort: String,
    val link: String,
)