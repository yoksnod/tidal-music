package com.example.app.entity

sealed class AlbumsResult {
    data class Success(val albums: List<Album>) : AlbumsResult()
    data class Fail(val exception: Exception) : AlbumsResult()
}
