package com.example.app.entity

data class TracksBody(val data: List<Track>)
