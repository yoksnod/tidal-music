package com.example.app.entity

data class AlbumsBody(val data: List<Album>, val next: String?, val total: Int)
