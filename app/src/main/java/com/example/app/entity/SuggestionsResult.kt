package com.example.app.entity

sealed class SuggestionsResult {
    data class Success(val searchSuggestions: List<SearchSuggestion>) : SuggestionsResult()
    data class Fail(val exception: Exception) : SuggestionsResult()
}

