package com.example.app.utils

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.app.app
import com.example.app.data.AlbumsRepo
import com.example.app.entity.Album
import com.example.app.viewmodel.AlbumDetailsViewModel
import com.example.app.viewmodel.AlbumsViewModel
import com.example.app.viewmodel.SearchSuggestionsViewModel
import com.example.app.viewmodel.TidalViewModelFactory

const val BUNDLE_ALBUM = "album"

// As an alternative it might be done using injection with some help from a DI framework,
// but as the project is fairly small, I didn't want to bring an heavyweight dependency to the project and wanted to keep it simpler
fun AppCompatActivity.makeSuggestionsViewModel(): SearchSuggestionsViewModel {
    return makeViewModel(SearchSuggestionsViewModel::class.java)
}

fun AppCompatActivity.makeAlbumsViewModel(): AlbumsViewModel {
    return makeViewModel(AlbumsViewModel::class.java)
}

fun AppCompatActivity.makeAlbumViewDetailsModel(album: Album): AlbumDetailsViewModel {
    return makeViewModel(AlbumDetailsViewModel::class.java, Bundle().apply { putParcelable(BUNDLE_ALBUM, album) })
}

fun <T : ViewModel> AppCompatActivity.makeViewModel(clazz: Class<T>, bundle: Bundle? = null): T {
    val repo: AlbumsRepo = app().albumsRepo
    val modelFactory = TidalViewModelFactory(
        repo,
        this,
        bundle
    )
    val modelProvider = ViewModelProvider(this, modelFactory)
    return modelProvider.get(clazz)
}

