package com.example.app.utils

import android.content.Context
import android.util.DisplayMetrics
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.app.tidal.R
import com.example.app.view.adapter.GridDecor
import kotlin.math.max

private const val MIN_SPAN_COUNT = 1

class ViewUtils private constructor() {

    companion object {

        @JvmStatic
        @BindingAdapter("imageUrl")
        fun loadImage(view: ImageView, url: String) {
            Glide.with(view.context)
                .load(url)
                .placeholder(android.R.drawable.ic_menu_myplaces)
                .error(android.R.drawable.stat_notify_error)
                .into(view)
        }

        @JvmStatic
        @BindingAdapter("rating")
        fun setRating(view: TextView, rating: Double) {
            view.text = view.resources.getString(R.string.rating, rating)
        }

        @JvmStatic
        @BindingAdapter("fans")
        fun setFans(view: TextView, fans: Long) {
            view.text = view.resources.getString(R.string.fans, fans)
        }
    }
}

fun View.setVisible() {
    visibility = View.VISIBLE
}

fun View.setGone() {
    visibility = View.GONE
}

fun calcColumnsCount(context: Context, resId: Int): Int {
    val columnWidth = context.resources.getDimensionPixelSize(resId)
    val displayMetrics: DisplayMetrics = context.resources.displayMetrics
    val screenWidth = displayMetrics.widthPixels * 1f
    return max(MIN_SPAN_COUNT, (screenWidth / columnWidth).toInt())
}

fun RecyclerView.setAlbumsConfiguration() {
    layoutManager = GridLayoutManager(context, calcColumnsCount(context, R.dimen.grid_item_size))
    addItemDecoration(GridDecor(resources.getDimensionPixelSize(R.dimen.grid_padding)))
}