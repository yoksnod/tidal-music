package com.example.app.data

import com.example.app.entity.AlbumDetails
import com.example.app.net.RestService

interface AlbumDetailsDataSource {
    suspend fun getAlbumDetails(id: Long): AlbumDetails
}

internal class AlbumDetailsDataSourceImpl(private val restService: RestService) : AlbumDetailsDataSource {
    override suspend fun getAlbumDetails(id: Long): AlbumDetails = restService.getAlbumDetails(id)
}