package com.example.app.data

import com.example.app.entity.SearchSuggestionsBody
import com.example.app.net.RestService

interface SearchSuggestionsDataSource {
    suspend fun getSuggestions(query: String): SearchSuggestionsBody
}

internal class SearchSuggestionsDataSourceImpl(
    private val restService: RestService
) : SearchSuggestionsDataSource {

    override suspend fun getSuggestions(query: String): SearchSuggestionsBody = restService.getSearchSuggestions(query)
}