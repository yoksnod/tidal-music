package com.example.app.data

import com.example.app.entity.AlbumsBody
import com.example.app.net.RestService

interface AlbumsDataSource {
    suspend fun getAlbums(id: Long): AlbumsBody
}

internal class AlbumsDataSourceImpl(private val restService: RestService) : AlbumsDataSource {
    override suspend fun getAlbums(id: Long): AlbumsBody = restService.getAlbums(id)
}