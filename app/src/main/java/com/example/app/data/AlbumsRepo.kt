package com.example.app.data

import com.example.app.entity.AlbumDetailsResult
import com.example.app.entity.AlbumsResult
import com.example.app.entity.SuggestionsResult

interface AlbumsRepo {
    suspend fun getSuggestions(query: String): SuggestionsResult
    suspend fun getAlbums(artistId: Long): AlbumsResult
    suspend fun getAlbumDetails(albumId: Long): AlbumDetailsResult
}

internal class AlbumsRepoImpl(
    private val searchSuggestionsDataSource: SearchSuggestionsDataSource,
    private val albumsDataSource: AlbumsDataSource,
    private val albumDetailsDataSource: AlbumDetailsDataSource
) : AlbumsRepo {

    override suspend fun getSuggestions(query: String): SuggestionsResult = try {
        SuggestionsResult.Success(searchSuggestionsDataSource.getSuggestions(query).data)
    } catch (e: Exception) {
        SuggestionsResult.Fail(e)
    }

    override suspend fun getAlbums(artistId: Long): AlbumsResult = try {
        AlbumsResult.Success(albumsDataSource.getAlbums(artistId).data)
    } catch (e: Exception) {
        AlbumsResult.Fail(e)
    }

    override suspend fun getAlbumDetails(albumId: Long): AlbumDetailsResult = try {
        AlbumDetailsResult.Success(albumDetailsDataSource.getAlbumDetails(albumId))
    } catch (e: Exception) {
        AlbumDetailsResult.Fail(e)
    }
}

