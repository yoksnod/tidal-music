package com.example.app.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app.entity.Album
import com.example.app.entity.AlbumDetails
import com.example.app.entity.AlbumDetailsResult
import com.example.app.tidal.R
import com.example.app.tidal.databinding.ActivityAlbumDetailsBinding
import com.example.app.tidal.databinding.HeaderContentBinding
import com.example.app.utils.BUNDLE_ALBUM
import com.example.app.utils.makeAlbumViewDetailsModel
import com.example.app.utils.setGone
import com.example.app.utils.setVisible
import com.example.app.view.adapter.ParallaxAdapterWrapper
import com.example.app.view.adapter.TracksAdapter
import com.example.app.viewmodel.AlbumDetailsViewModel
import com.google.android.material.snackbar.Snackbar

class AlbumDetailsViewActivity : AppCompatActivity() {

    private lateinit var mainBinding: ActivityAlbumDetailsBinding
    private lateinit var headerBinding: HeaderContentBinding
    private lateinit var detailsModel: AlbumDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mainBinding = DataBindingUtil.setContentView(this, R.layout.activity_album_details)
        val album: Album = intent.extras?.getParcelable(BUNDLE_ALBUM) ?: throw RuntimeException("album must be not null")

        mainBinding.recycler.layoutManager = LinearLayoutManager(this)
        headerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.header_content, mainBinding.recycler, false)
        detailsModel = makeAlbumViewDetailsModel(album)

        detailsModel.detailsLiveData?.observe(this, { onDetailsReceived(it) }) ?: requestDetails()
    }

    private fun requestDetails() {
        detailsModel.requestAlbumDetails().observe(this, {
            onDetailsReceived(it)
        })
    }

    private fun onDetailsReceived(detailsResult: AlbumDetailsResult) = when (detailsResult) {
        is AlbumDetailsResult.Success -> onSuccess(detailsResult)
        is AlbumDetailsResult.Fail -> onFail(detailsResult)
    }

    private fun onSuccess(result: AlbumDetailsResult.Success) {
        mainBinding.progress.setGone()
        mainBinding.detailsContent.setVisible()

        headerBinding.details = result.details
        mainBinding.recycler.setParallaxAdapter(result.details, headerBinding, mainBinding)
    }

    private fun onFail(detailsResult: AlbumDetailsResult.Fail) {
        mainBinding.progress.setGone()
        mainBinding.detailsContent.setVisible()
        Snackbar.make(mainBinding.content, "${detailsResult.exception}", Snackbar.LENGTH_INDEFINITE).setAction(R.string.retry) { onRetry() }.show()
    }

    private fun onRetry() {
        mainBinding.progress.setVisible()
        mainBinding.detailsContent.setGone()
        requestDetails()
    }

    companion object {

        fun buildIntent(activity: Activity, album: Album) = Intent(activity, AlbumDetailsViewActivity::class.java).apply {
            putExtra(BUNDLE_ALBUM, album)
        }
    }
}

private fun RecyclerView.setParallaxAdapter(details: AlbumDetails, headerBinding: HeaderContentBinding, mainBinding: ActivityAlbumDetailsBinding) {
    val tracksAdapter = TracksAdapter(details.tracks.data)
    val parallaxAdapter = ParallaxAdapterWrapper(tracksAdapter)
    parallaxAdapter.setParallaxHeader(headerBinding.root, mainBinding.recycler)
    adapter = parallaxAdapter
}


