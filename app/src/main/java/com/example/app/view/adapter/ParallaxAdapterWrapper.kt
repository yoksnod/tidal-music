package com.example.app.view.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.app.entity.Track
import com.poliveira.parallaxrecyclerview.ParallaxRecyclerAdapter

class ParallaxAdapterWrapper(private val wrapped: TracksAdapter) : ParallaxRecyclerAdapter<Track>(wrapped.tracks) {
    override fun onBindViewHolderImpl(viewHolder: RecyclerView.ViewHolder, adapter: ParallaxRecyclerAdapter<Track>?, i: Int) {
        wrapped.onBindViewHolder(viewHolder as TrackHolder, i)
    }

    override fun onCreateViewHolderImpl(viewGroup: ViewGroup, adapter: ParallaxRecyclerAdapter<Track>?, i: Int): RecyclerView.ViewHolder {
        return wrapped.onCreateViewHolder(viewGroup, i)
    }

    override fun getItemCountImpl(adapter: ParallaxRecyclerAdapter<Track>?): Int {
        return wrapped.itemCount
    }
}