package com.example.app.view.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.app.entity.SearchSuggestion

class SuggestionsAdapter(activity: Activity, val items: List<SearchSuggestion>) : ArrayAdapter<SearchSuggestion>(
    activity, 0, items
) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getViewInternal(convertView = convertView, parent = parent, position = position)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getViewInternal(convertView = convertView, parent = parent, position = position)
    }

    private fun getViewInternal(convertView: View?, parent: ViewGroup, position: Int): View {
        val holder: ViewHolder
        val root: View = convertView ?: makeHolderItemView(parent)
        holder = root.tag as ViewHolder
        holder.title.text = items[position].title
        return root
    }

    private fun makeHolderItemView(parent: ViewGroup): View {
        val itemView = LayoutInflater.from(parent.context).inflate(
            android.R.layout.simple_dropdown_item_1line,
            parent,
            false
        )
        return itemView.apply { tag = ViewHolder(this) }
    }

    class ViewHolder(parent: View) {
        val title = parent.findViewById(android.R.id.text1) as TextView
    }
}
