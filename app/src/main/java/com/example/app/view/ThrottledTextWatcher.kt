package com.example.app.view

import android.text.Editable
import android.text.TextWatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private const val DELAY = 500L

class ThrottledTextWatcher(
    private val coroutineScope: CoroutineScope,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    onTextChanged: (String) -> Unit
) : TextWatcher {
    private var onTextChanged: ((String) -> Unit)? = onTextChanged
    private var job: Job? = null
    private var lastQuery: String? = null

    override fun afterTextChanged(s: Editable?) {}

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        lastQuery = s?.toString()
        if (hasJobInProgress()) return
        job = coroutineScope.launch {
            delayBlocking(ioDispatcher)
            lastQuery?.trim().toString().takeUnless { it.isEmpty() }?.let { query -> onTextChanged?.let { it(query) } }
        }
    }

    fun detach() {
        onTextChanged = null
    }

    private fun hasJobInProgress() = job?.isCompleted == false

    companion object {
        private suspend fun delayBlocking(ioDispatcher: CoroutineDispatcher) = withContext(ioDispatcher) { delay(DELAY) }
    }
}