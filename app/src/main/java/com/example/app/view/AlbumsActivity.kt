package com.example.app.view

import android.os.Bundle
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import com.example.app.entity.Album
import com.example.app.entity.AlbumsResult
import com.example.app.entity.SearchSuggestion
import com.example.app.entity.SuggestionsResult
import com.example.app.tidal.R
import com.example.app.tidal.databinding.ActivityMainBinding
import com.example.app.utils.makeAlbumsViewModel
import com.example.app.utils.makeSuggestionsViewModel
import com.example.app.utils.setAlbumsConfiguration
import com.example.app.utils.setGone
import com.example.app.utils.setVisible
import com.example.app.view.adapter.AlbumsAdapter
import com.example.app.view.adapter.SuggestionsAdapter
import com.example.app.viewmodel.AlbumsViewModel
import com.example.app.viewmodel.SearchSuggestionsViewModel
import com.google.android.material.snackbar.Snackbar

class AlbumsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var suggestionsViewModel: SearchSuggestionsViewModel
    private lateinit var albumsViewModel: AlbumsViewModel
    private lateinit var textWatcher: ThrottledTextWatcher

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        suggestionsViewModel = makeSuggestionsViewModel()
        albumsViewModel = makeAlbumsViewModel()
        binding.recycler.setAlbumsConfiguration()

        textWatcher = ThrottledTextWatcher(lifecycleScope) { requestSuggestions(it) }
        binding.autoCompleteView.addTextChangedListener(textWatcher)
        albumsViewModel.albumsLiveData?.observe(this, { onAlbumsReceived(it) })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        textWatcher.detach()
    }

    private fun requestSuggestions(search: String) {
        suggestionsViewModel.requestSuggestions(search).observe(this, { onSuggestionsReceived(it) })
    }

    private fun onSuggestionsReceived(result: SuggestionsResult) = when (result) {
        is SuggestionsResult.Success -> onSuggestionsSucceeded(result)
        is SuggestionsResult.Fail -> onSuggestionsFailed(result)
    }

    private fun onSuggestionsSucceeded(result: SuggestionsResult.Success) {
        val suggestionsAdapter = SuggestionsAdapter(this, result.searchSuggestions)
        binding.autoCompleteView.setAdapter(suggestionsAdapter)
        suggestionsAdapter.notifyDataSetChanged()
        binding.autoCompleteView.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            requestAlbums(suggestionsAdapter.items[position])
        }
    }

    private fun requestAlbums(suggestion: SearchSuggestion) {
        binding.progress.setVisible()
        binding.recycler.setGone()
        albumsViewModel.requestAlbums(suggestion.artist.id).observe(this, { onAlbumsReceived(it) })
    }

    private fun onSuggestionsFailed(result: SuggestionsResult.Fail) {
        Snackbar.make(binding.content, "${result.exception}", Snackbar.LENGTH_LONG).show()
    }

    private fun onAlbumsReceived(result: AlbumsResult) = when (result) {
        is AlbumsResult.Success -> onGetAlbumsSuccess(result)
        is AlbumsResult.Fail -> onGetAlbumsFail(result)
    }

    private fun onGetAlbumsSuccess(success: AlbumsResult.Success) {
        binding.progress.setGone()
        binding.recycler.setVisible()
        binding.recycler.adapter = AlbumsAdapter(success.albums) { openNextScreen(it) }
    }

    private fun onGetAlbumsFail(fail: AlbumsResult.Fail) {
        binding.progress.setGone()
        binding.recycler.setGone()
        Snackbar.make(binding.content, "${fail.exception}", Snackbar.LENGTH_LONG).show()
    }

    private fun openNextScreen(album: Album) {
        startActivity(AlbumDetailsViewActivity.buildIntent(this, album))
    }
}
