package com.example.app.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.app.entity.Track
import com.example.app.tidal.R
import com.example.app.tidal.databinding.TrackModelBinding

class TracksAdapter(internal val tracks: List<Track>) : RecyclerView.Adapter<TrackHolder>() {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): TrackHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val binding: TrackModelBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.track_model,
            viewGroup,
            false
        )
        return TrackHolder(binding)
    }

    override fun onBindViewHolder(trackHolder: TrackHolder, position: Int) {
        trackHolder.binding.track = getItem(position)
    }

    override fun getItemCount(): Int = tracks.size

    private fun getItem(position: Int): Track = tracks[position]
}

class TrackHolder(val binding: TrackModelBinding) : RecyclerView.ViewHolder(binding.root)