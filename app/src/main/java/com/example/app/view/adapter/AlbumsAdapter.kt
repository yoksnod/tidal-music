package com.example.app.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.app.entity.Album
import com.example.app.tidal.R
import com.example.app.tidal.databinding.AlbumModelBinding

class AlbumsAdapter(
    private val albums: List<Album>,
    private val onItemClicked: (Album) -> Unit
) : RecyclerView.Adapter<AlbumsAdapter.AlbumHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): AlbumHolder {
        val inflater = LayoutInflater.from(viewGroup.context)
        val binding: AlbumModelBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.album_model,
            viewGroup,
            false
        )
        return AlbumHolder(binding, onItemClicked)
    }

    override fun onBindViewHolder(albumHolder: AlbumHolder, position: Int) {
        albumHolder.binding.album = getItem(position)
    }

    override fun getItemCount(): Int = albums.size

    private fun getItem(position: Int): Album = albums[position]

    class AlbumHolder(
        val binding: AlbumModelBinding,
        private val onItemClicked: (Album) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener { binding.album?.let { onItemClicked(it) } }
        }
    }
}