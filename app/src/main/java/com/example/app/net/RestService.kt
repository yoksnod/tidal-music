package com.example.app.net

import com.example.app.entity.AlbumDetails
import com.example.app.entity.AlbumsBody
import com.example.app.entity.SearchSuggestionsBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RestService {
    @GET("/search")
    suspend fun getSearchSuggestions(@Query("q") query: String): SearchSuggestionsBody

    @GET("/artist/{id}/albums")
    suspend fun getAlbums(@Path("id") id: Long): AlbumsBody

    @GET("/album/{id}")
    suspend fun getAlbumDetails(@Path("id") id: Long): AlbumDetails
}