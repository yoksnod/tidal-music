package com.example.app.net

import android.net.Uri
import com.example.app.App
import com.example.app.tidal.R
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RestProvider(app: App) {
    val restService: RestService

    init {
        val client = createHttpClient(app)
        val gson = GsonBuilder().create()
        val gsonConverterFactory = GsonConverterFactory.create(gson)
        restService = createRestClient(app, client, gsonConverterFactory)
    }

    companion object {
        private const val CACHE_SIZE = 10 * 10 * 1024L
        private const val CONNECTION_TIMEOUT = 10L
        private const val READ_TIMEOUT = 10L

        private fun createRestClient(
            app: App,
            client: OkHttpClient,
            gsonConverterFactory: GsonConverterFactory
        ): RestService {
            val uri = Uri.Builder()
                .authority(app.getString(R.string.app_def_host))
                .scheme(app.getString(R.string.app_def_scheme))
                .build()
            return Retrofit.Builder()
                .client(client)
                .baseUrl(uri.toString())
                .validateEagerly(true)
                .addConverterFactory(gsonConverterFactory)
                .build()
                .create(RestService::class.java)
        }

        private fun createHttpClient(app: App): OkHttpClient = OkHttpClient
            .Builder()
            .cache(Cache(app.cacheDir, CACHE_SIZE))
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .build()
    }
}